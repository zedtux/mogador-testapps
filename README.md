# Mogador test Apps

This repository stores the test applications used in order to test the mogador integration in [mogador templates](https://gitlab.com/zedtux/mogador-templates).

See [the Mogador gem](https://gitlab.com/zedtux/mogador) for more details.
