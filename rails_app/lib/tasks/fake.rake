# frozen_string_literal: true

namespace :fake do
  desc 'Generates 10 random users'
  task ten_random_users_baby: :environment do
    10.times { User.create!(name: FFaker::Name.name) }
  end
end
