# Rails test app

This application is used in order to test the Rails Mogador templates.

See [the mogador gem](https://gitlab.com/zedtux/mogador) and [its templates](https://gitlab.com/zedtux/mogador-templates) for more information.
