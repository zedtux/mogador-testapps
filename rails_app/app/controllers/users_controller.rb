# frozen_string_literal: true

class UsersController < ApplicationController
  def index
    @users = User.all
    Rails.logger.debug "[debug(#{__FILE__.split('app/')[1]}:#{__LINE__})] @users: #{@users.inspect}"
  end
end
